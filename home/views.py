
from django.shortcuts import render
from django.http import HttpResponse

import datetime
from django.utils import timezone as tz
from datetime import timedelta

# -*- coding: utf-8 -*-


from django.shortcuts import render

# Import Datetime
from datetime import datetime

# Create your views here.
def index (request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')


def time(request):
    args = {}
    now = tz.now()
    args['sekarang'] = now
    return render(request, 'time.html', args)

def time_mod(request, id):
    args={}
    now = tz.now()
    waktu = now + timedelta(hours=id)

    args['sekarang'] = waktu
    return render(request, 'time.html', args)